package com.macro.mall.tiny.controller;


import com.macro.mall.tiny.dao.AccountDao;
import com.macro.mall.tiny.domin.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

//登录验证
@Controller
public class LoginCheckController {
    @Autowired
    private AccountDao accountDao;

    @RequestMapping("/loginCheck")
    public ModelAndView longinCheck(ModelAndView modelAndView, Account account){
        System.out.println(account);
        if (account.getUsername()==null||account.getUsername()==""){
            modelAndView.addObject("msg","用户名不能为空");
            modelAndView.setViewName("login");
        }else if (account.getPassword()==null||account.getPassword()==""){
            modelAndView.addObject("msg","用户密码不能为空");
            modelAndView.setViewName("login");
        }else {
            Account accountByUP = accountDao.getAccountByUP(account);
            if (null!=accountByUP){
                modelAndView.setViewName("index");
            }
        }
        return modelAndView;
    }
}
