package com.macro.mall.tiny.dao;


import com.macro.mall.tiny.domin.Account;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;


@Repository
public class AccountDao {

    private static Map<Integer, Account> accountMap = null;
//模拟数据库数据
    static {
        accountMap=new HashMap<>();//

        accountMap.put(2001,new Account(2001,"admin","admin123"));
    }

    //模拟主键自增长
    private static Integer initID=2002;

    //根据id获取account
    public Account getAccountById(Integer id){
        return accountMap.get(id);
    }
    //添加account
    public void addAccount(Account account){
        accountMap.put(initID++,account);
    }

    //根据username和password获取account
    public Account getAccountByUP(Account account){
        for (Integer integer : accountMap.keySet()) {
            Account accountOut = accountMap.get(integer);
            if (accountOut.equals(account)){
                return accountOut;
            }
        }
        return null;
    }
}
