package com.macro.mall.tiny.config;

import com.macro.mall.tiny.utils.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

//自定义国家地区解析
public class MyLocaleResolver implements LocaleResolver {
    //解析请求
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        //获取请求中的语言参数
        String language = request.getParameter("lang");
        Locale locale = Locale.getDefault();//如果没有就使用默认了
        //如果请求的链接携带了国际化参数
        if (!StringUtils.isEmpty(language)){
            //分割成国家和地区
            String[] s = language.split("_");
            locale=new Locale(s[0],s[1]);
        }
        setLocale(request,null,locale);
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}
